'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    static associate(models) {
        Users.hasMany(models.Scores, {foreignKey: 'id_user'});
        Users.hasMany(models.Reports, {foreignKey: 'id_user'});
    }
  };
  Users.init({
    id_user: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    username: {
        type: DataTypes.STRING(200),
        require: true,
        unique: true
    },
    email: {
        type: DataTypes.STRING(200),
        require: true,
        unique: true
    },
    password: {
        type: DataTypes.STRING(255),
        require: true,
        min: 6
    },
    sponsor: {
        type: DataTypes.STRING(30),
        require: true
    },
    avatar: {
        type: DataTypes.STRING()
    },
    role: {
        type: DataTypes.STRING(30)
    },
    point: {
        type: DataTypes.INTEGER(5)
    },
    active: {
        type: DataTypes.BOOLEAN
    }
  }, {
    sequelize,
    modelName: 'Users',
  });
  return Users;
};