'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Match extends Model {
    static associate(models) {
      Match.hasMany(models.Scores, { foreignKey: "id_match" });
    }
  };
  Match.init({
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    home: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    away: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    logo_home: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    logo_away: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    score_home: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    score_away: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    venue: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    referee: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    date: {
      type: DataTypes.DATE,
    },
    day: {
      type: DataTypes.INTEGER,
    },
  }, {
    sequelize,
    modelName: 'Match',
  });
  return Match;
};