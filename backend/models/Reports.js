'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Reports extends Model {
    static associate(models) {
      Reports.belongsTo(models.Users, {foreignKey: 'id_user'});
    }
  };
  Reports.init({
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    report: {
      type: DataTypes.STRING,
      require: true
    },
    id_user: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    read: {
      type: DataTypes.BOOLEAN,
      require: true,
      defaultValue: 0,
    }
  }, {
    sequelize,
    modelName: 'Reports',
  });
  return Reports;
};