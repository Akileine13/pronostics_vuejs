'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Scores extends Model {
    static associate(models) {
      Scores.belongsTo(models.Users, {foreignKey: 'id_user'});
      Scores.belongsTo(models.Match, {foreignKey: 'id_match'});
    }
  };
  Scores.init({
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    home: {
      type: DataTypes.INTEGER
    },
    away: {
      type: DataTypes.INTEGER
    },
  }, {
    sequelize,
    modelName: 'Scores',
  });
  return Scores;
};