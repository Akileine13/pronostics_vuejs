'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Notifications extends Model {
    static associate(models) {
      
    }
  };
  Notifications.init({
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    message: {
      type: DataTypes.STRING,
      allowNull: true,
      require: true
    },
    important: {
      type: DataTypes.STRING(30),
      allowNull: true,
      require: true
    }
  }, {
    sequelize,
    modelName: 'Notifications',
  });
  return Notifications;
};