require("dotenv").config();
const {DB_HOST, DB_NAME, DB_USER, DB_PASSWORD} = process.env

// CHANGE IF LINUX
const linux = false
const windows = 1000
// const mac = 8889
// const linux = 3306

module.exports = {
  development: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: DB_NAME,
    host: DB_HOST,
    port: windows,
    socketPath: linux ? '/var/run/mysqld/mysqld.sock' : '/Applications/MAMP/tmp/mysql/mysql.sock', 
    dialect: "mysql"
  },
  test: {
    username: "root",
    password: null,
    database: "database_test",
    host: "127.0.0.1",
    dialect: "mysql"
  },
  production: {
    username: "root",
    password: null,
    database: "database_production",
    host: "127.0.0.1",
    dialect: "mysql"
  }
}
