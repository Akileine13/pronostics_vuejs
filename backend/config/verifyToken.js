const jwt = require('jsonwebtoken')
const { ACCESS_TOKEN_SECRET } = process.env

function verifyToken(req, res, next) {
    const token = req.header('auth-token')
    if (!token) return res.status(401).send('Accés refusé.')

    try {
        const verified = jwt.verify(token, ACCESS_TOKEN_SECRET)
        req.user = verified
        next()
    } catch (error) {
        res.status(400).send("Invalid token")
    }

}

module.exports = verifyToken