const router = require("express").Router();
const userController = require("../controllers/userController");
const notificationController = require("../controllers/notificationController");
const reportController = require("../controllers/reportController");
const scoreController = require("../controllers/scoreController");
const matchController = require("../controllers/matchController");
const verifyToken = require("../config/verifyToken");

// Home
router.post("/register", userController.createUser);
router.post("/login", userController.login);

// Match 
router.post('/match/:id', verifyToken, matchController.updateMatch)
router.get('/match', verifyToken, matchController.getMatchByDate)
router.get('/match-day', verifyToken, matchController.getMatchByDay)

// User
router.get("/user/:id_user", verifyToken, userController.getUser)
router.get("/user-point", verifyToken, userController.getUserPoint)
router.get("/user-id", verifyToken, userController.getUserId)
router.get("/number-user", verifyToken, userController.getNumberUser)
router.post('/update-profile/:id_user', verifyToken, userController.updateProfile)
router.get("/users-inactive", verifyToken, userController.getUserInactive)
router.put("/users-active/:id_user", verifyToken, userController.updateUserInActive)

router.delete("/destroy-user-inactive/:id_user", verifyToken, userController.destroyUserInactive)

// Notification
router.get("/get-notification", verifyToken, notificationController.getNotification);
router.post("/send-notification", notificationController.sendNotification);

// Report
router.get("/get-report", verifyToken, reportController.getReport);
router.get("/get-report/:id_user", verifyToken, reportController.getReportPerUser);
router.post("/send-report", verifyToken, reportController.sendReport);

// Score
router.post("/send-results/:id", verifyToken, scoreController.sendResults);
router.put("/purge", verifyToken, scoreController.purgeResults);
router.get("/get-response/:id", verifyToken, scoreController.getIfResponse);
router.get("/get-score/:id", verifyToken, scoreController.getScore);

module.exports = router;
