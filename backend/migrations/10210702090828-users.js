"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable("Users", {
      id_user: {
        primaryKey: true,
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true
      },
      username: {
        type: Sequelize.STRING(200),
        require: true,
        unique: true,
      },
      email: {
        type: Sequelize.STRING(200),
        require: true,
        unique: true,
      },
      password: {
        type: Sequelize.STRING(255),
        require: true,
        min: 6,
      },
      sponsor: {
        type: Sequelize.STRING(30),
        require: true,
      },
      avatar: {
        type: Sequelize.STRING(),
      },
      role: {
        type: Sequelize.STRING(30),
      },
      point: {
        type: Sequelize.INTEGER(5),
      },
      createdAt: {
        allowNull: false,
        defaultValue: Sequelize.NOW,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        defaultValue: Sequelize.NOW,
        type: Sequelize.DATE,
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable("Users", {
      id_user: {
        primaryKey: true,
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true
      },
      username: {
        type: Sequelize.STRING(200),
        require: true,
        unique: true,
      },
      email: {
        type: Sequelize.STRING(200),
        require: true,
        unique: true,
      },
      password: {
        type: Sequelize.STRING(255),
        require: true,
        min: 6,
      },
      sponsor: {
        type: Sequelize.STRING(30),
        require: true,
      },
      avatar: {
        type: Sequelize.STRING(),
      },
      role: {
        type: Sequelize.STRING(30),
      },
      point: {
        type: Sequelize.INTEGER(5),
      },
      createdAt: {
        allowNull: false,
        defaultValue: Sequelize.NOW,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        defaultValue: Sequelize.NOW,
        type: Sequelize.DATE,
      },
    });
  },
};
