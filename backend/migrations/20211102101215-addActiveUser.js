'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Users', 'active', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false,
      after: 'point'
    })
  },
  down: async (queryInterface) => {
    return queryInterface.removeColumn('Users', 'active')
  }
};