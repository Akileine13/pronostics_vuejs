"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface
      .createTable("Notifications", {
        id: {
          primaryKey: true,
          type: Sequelize.INTEGER,
          allowNull: false,
          autoIncrement: true
        },
        message: {
          type: Sequelize.STRING,
          allowNull: true,
          require: true
        },
        important: {
          type: Sequelize.STRING(30),
          allowNull: true,
          require: true
        },
        createdAt: {
          allowNull: false,
          defaultValue: Sequelize.NOW,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          defaultValue: Sequelize.NOW,
          type: Sequelize.DATE,
        },
      })
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface
      .dropTable("Notifications", {
        id: {
          primaryKey: true,
          type: Sequelize.INTEGER,
          allowNull: false,
          autoIncrement: true
        },
        message: {
          type: Sequelize.STRING,
          allowNull: true,
          require: true
        },
        important: {
          type: Sequelize.STRING(30),
          allowNull: true,
          require: true
        },
        createdAt: {
          allowNull: false,
          defaultValue: Sequelize.NOW,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          defaultValue: Sequelize.NOW,
          type: Sequelize.DATE,
        },
      })
  },
};
