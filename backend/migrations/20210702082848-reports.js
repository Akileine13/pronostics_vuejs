'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface
      .createTable("Reports", {
        id: {
          primaryKey: true,
          type: Sequelize.INTEGER,
          allowNull: false,
          autoIncrement: true
        },
        report: {
          type: Sequelize.STRING,
          require: true
        },
        id_user: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'Users',
            key: 'id_user'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        },
        read: {
          type: Sequelize.BOOLEAN,
          require: true,
          defaultValue: 0,
        },
        createdAt: {
          allowNull: false,
          defaultValue: Sequelize.NOW,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          defaultValue: Sequelize.NOW,
          type: Sequelize.DATE,
        },
      })
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface
      .dropTable("Reports", {
        id: {
          primaryKey: true,
          type: Sequelize.INTEGER,
          allowNull: false,
          autoIncrement: true
        },
        report: {
          type: Sequelize.STRING,
          require: true
        },
        id_user: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'Users',
            key: 'id_user'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        },
        read: {
          type: Sequelize.BOOLEAN,
          require: true,
          defaultValue: 0,
        },
        createdAt: {
          allowNull: false,
          defaultValue: Sequelize.NOW,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          defaultValue: Sequelize.NOW,
          type: Sequelize.DATE,
        },
      })
  }
};
