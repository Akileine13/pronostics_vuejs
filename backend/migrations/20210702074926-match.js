"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface
      .createTable("Matches", {
        id: {
          primaryKey: true,
          type: Sequelize.INTEGER,
          allowNull: false,
          autoIncrement: true,
        },
        home: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        away: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        logo_home: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        logo_away: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        score_home: {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
        score_away: {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
        venue: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        referee: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        date: {
          type: Sequelize.DATE,
          allowNull: true,
        },
        day: {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
        createdAt: {
          allowNull: false,
          defaultValue: Sequelize.NOW,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          defaultValue: Sequelize.NOW,
          type: Sequelize.DATE,
        },
      })
      .then(() => {
        const list = []

        for (let i = 1; i < 11; i++) {
          list.push({
            id: i,
            day: i,
            createdAt: new Date(),
            updatedAt: new Date()
          })
        }

        return queryInterface.bulkInsert("Matches", list);
      });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface
      .dropTable("Matches", {
        id: {
          primaryKey: true,
          type: Sequelize.INTEGER,
          allowNull: false,
          autoIncrement: true
        },
        home: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        away: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        logo_home: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        logo_away: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        score_home: {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
        score_away: {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
        venue: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        referee: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        date: {
          type: Sequelize.DATE,
        },
        day: {
          type: Sequelize.INTEGER,
        },
        createdAt: {
          allowNull: false,
          defaultValue: Sequelize.NOW,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          defaultValue: Sequelize.NOW,
          type: Sequelize.DATE,
        },
      })
      .then(() => {
        for (let i = 1; i < 11; i++) {
          return queryInterface.bulkDelete("matches", [
            {
              id: i,
              day: i,
            },
          ]);
        }
      });
  },
};
