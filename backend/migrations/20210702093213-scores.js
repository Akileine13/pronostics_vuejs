'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface
      .createTable("Scores", {
        id: {
          primaryKey: true,
          type: Sequelize.INTEGER,
          allowNull: false,
          autoIncrement: true
        },
        home: {
          type: Sequelize.INTEGER
        },
        away: {
          type: Sequelize.INTEGER
        },
        id_match: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'Matches',
            key: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        },
        id_user: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'Users',
            key: 'id_user'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        },
        createdAt: {
          allowNull: false,
          defaultValue: Sequelize.NOW,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          defaultValue: Sequelize.NOW,
          type: Sequelize.DATE,
        },
      })
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface
      .dropTable("Scores", {
        id: {
          primaryKey: true,
          type: Sequelize.INTEGER,
          allowNull: false,
          autoIncrement: true
        },
        home: {
          type: Sequelize.INTEGER
        },
        away: {
          type: Sequelize.INTEGER
        },
        id_match: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'Matches',
            key: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        },
        id_user: {
          type: Sequelize.INTEGER,
          allowNull: false,
          references: {
            model: 'Users',
            key: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        },
        createdAt: {
          allowNull: false,
          defaultValue: Sequelize.NOW,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          defaultValue: Sequelize.NOW,
          type: Sequelize.DATE,
        },
      })
  }
};
