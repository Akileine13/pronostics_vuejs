const Notifications = require("../models").Notifications;

exports.getNotification = (req, res) => {
    Notifications.findAll({
      attributes: [
        'message',
        'important'
      ],
      limit: 5,
      order: [['createdAt', 'DESC']]
    })
    .then((results) => {
        res.status(200).send(results);
      })
    .catch((err) => {
        res.status(500).send(err);
    })
}

exports.sendNotification = (req, res) => {
  Notifications.create({
      message: req.body.message,
      important: req.body.selected
  })
  .then((results) => {
      res.status(200).send(results);
    })
  .catch((err) => {
      res.status(500).send(err);
  })
}