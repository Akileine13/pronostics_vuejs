const Users = require("../models").Users;
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { ACCESS_TOKEN_SECRET } = process.env;

exports.createUser = async (req, res) => {
  const { username, email, password, passwordConfirm, sponsor } = req.body;

  if (!username)
    return res.status(400).send("Le pseudo ne doit pas être vide.");
  if (!email) return res.status(400).send("L'email ne doit pas être vide.");
  if (!password)
    return res.status(400).send("Le mot de passe ne doit pas être vide.");
  if (!passwordConfirm)
    return res
      .status(400)
      .send("La confirmation du mot de passe ne doit pas être vide.");
  // A corriger
  if (password.lenght < 5)
    return res
      .status(400)
      .send("Le mot de passe doit avoir au minimum 6 caractères.");
  if (password !== passwordConfirm)
    return res.status(400).send("Les mots de passe ne sont pas identiques.");
  if (!sponsor)
    return res.status(400).send("Le sponsor ne doit pas être vide.");

  const usernameExist = await Users.findOne({
    where: { username: username },
  });
  if (usernameExist)
    return res.status(400).send("Ce pseudo n'est plus disponible.");

  const emailExist = await Users.findOne({ where: { email: email } });
  if (emailExist)
    return res.status(400).send("Cet email est déjà lié à un compte.");

  if (sponsor.toLowerCase() !== 'pierre') 
    return res.status(400).send("Le code parrain n'est pas bon.");

  const hashPass = await bcrypt.hash(password, 10);

  Users.create({
    username: username,
    email: email,
    password: hashPass,
    sponsor: sponsor,
    avatar: "https://media.api-sports.io/football/teams/81.png",
    role: username === "Akileine13" ? "admin" : "users",
    point: 0,
    active: username === "Akileine13" ? 1 : 0,
  })
    .then((results) => {
      res.status(200).send(results);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
};

exports.login = async (req, res) => {
  const { username, password } = req.body;

  if (!username)
    return res.status(400).send("Le pseudo ne doit pas être vide.");
  if (!password)
    return res.status(400).send("Le mot de passe ne doit pas être vide.");

  const usernameExist = await Users.findOne({
    where: { username: username },
  });
  if (!usernameExist) return res.status(400).send("Ce pseudo n'existe pas.");

  const passwordIsCorrect = await bcrypt.compare(
    password,
    usernameExist.dataValues.password
  );
  if (!passwordIsCorrect) return res.status(400).send("Mot de passe invalide.");

  const isActive = usernameExist.dataValues.active
  if (!isActive) return res.status(400).send("Votre compte n'est pas encore activé.");

  const token = jwt.sign(
    { _id_user: usernameExist.dataValues.id_user, _role: usernameExist.dataValues.role },
    ACCESS_TOKEN_SECRET,
    { expiresIn: "3600s" }
  );
  res.header("auth-token", token).send(token);
};

exports.getUser = (req, res) => {
  const { id_user } = req.params;
  Users.findAll({
    attributes: [
      "id_user",
      "username",
      "email",
      "password",
      "point",
      "role",
      "avatar",
    ],
    where: {
      id_user: id_user,
    },
  })
    .then((results) => {
      res.status(200).send(results);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
};

exports.getUserPoint = (req, res) => {
  Users.findAll({
    attributes: ["id_user", "username", "point", "avatar", "role"],
    order: [["point", "DESC"]],
    where: {
      active: 1
    }
  })
    .then((results) => {
      res.status(200).send(results);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
};

exports.getUserId = (req, res) => {
  Users.findAll({
    attributes: ["id_user"],
    order: [["id_user", "DESC"]],
    limit: 1,
  })
    .then((results) => {
      res.status(200).send(results);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
};

exports.updateProfile = async (req, res) => {
  const { email, password, avatar } = req.body;
  const { id_user } = req.params;

  const hashPass = await bcrypt.hash(password, 10);

  Users.update(
    {
      email: email,
      avatar: avatar,
    },
    {
      where: {
        id_user: id_user,
      },
    }
  )
    .then((results) => {
      res.status(200).send(results);
    })
    .catch((err) => {
      res.status(500).send(err);
    });

    if(password !== '') {
      Users.update(
        {
          password: hashPass,
        },
        {
          where: {
            id_user: id_user,
          },
        }
      )
        .then((results) => {
          return res.status(200).send(results);
        })
        .catch((err) => {
          res.status(500).send(err);
        });
    }
    
};

exports.getUserInactive = (req, res) => {
  Users.findAll({
    attributes: ["id_user", "username", "email", "sponsor"],
    where: {
      active: 0
    }
  })
    .then((results) => {
      res.status(200).send(results);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
};

exports.updateUserInActive = (req, res) => {
  const { active } = req.body;
  const { id_user } = req.params;

  Users.update({
    active: active,
  },
  {
    where: {
      id_user: id_user
    }
  }
  )
    .then((results) => {
      res.status(200).send(results);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
};

exports.destroyUserInactive = (req, res) => {
  const { id_user } = req.params;

  Users.destroy(
  {
    where: {
      id_user: id_user
    }
  }
  )
    .then(() => {
      res.status(200).send();
    })
    .catch((err) => {
      res.status(500).send(err);
    });
};

exports.getNumberUser = (req, res) => {
  Users.findAll({
    attributes: ["id_user"],
  })
    .then((results) => {
      res.status(200).send(results);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
};
