const Match = require("../models").Match;

exports.getMatchByDate = (req, res) => {
  Match.findAll({
    order: [['date', 'ASC']]
  })
    .then((results) => {
      res.status(200).send(results);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
};

exports.getMatchByDay = (req, res) => {
  Match.findAll({
    attributes: ['day'],
    limit: 1
  })
    .then((results) => {
      res.status(200).send(results);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
};

exports.updateMatch = (req, res) => {
  const { allFixtures } = req.body;
  for (let i = 1; i < 11; i++) {
    Match.update(
      {
        home: allFixtures[i - 1].home,
        away: allFixtures[i - 1].away,
        logo_home: allFixtures[i - 1].homeLogo,
        logo_away: allFixtures[i - 1].awayLogo,
        referee: allFixtures[i - 1].referee,
        venue: allFixtures[i - 1].venue,
        date: allFixtures[i - 1].date,
        score_home: allFixtures[i - 1].score.home,
        score_away: allFixtures[i - 1].score.away,
        day: allFixtures[i - 1].day,
      },
      {
        where: {
          id: i,
        },
      }
    )
      .then(() => {
        if (i === 10) res.status(200).send("Match à jour.");
      })
      .catch((err) => {
        res.status(500).send(err);
      });
  }
};
