const Reports = require("../models").Reports;
const Users = require("../models").Users;
exports.getReport = (req, res) => {
  Reports.findAll({
    order: [['createdAt', 'DESC']],
    attributes: [
      'report',
      'id_user',
      'read'
    ],
    where: {
      read: 0
    },
    include: [{
      model: Users,
      attributes: [
        'username'
      ]
    }]
  })
    .then((results) => {
      res.status(200).send(results);
    })
    .catch((err) => {
      res.status(500).send(err);
    })
}

exports.getReportPerUser = (req, res) => {
  Reports.findAll({
    order: [['createdAt', 'DESC']],
    attributes: [
      'report',
      'id_user',
      'read'
    ],
    where: {
      id_user: req.params.id_user
    }
  })
    .then((results) => {
      res.status(200).send(results);
    })
    .catch((err) => {
      res.status(500).send(err);
    })
}

exports.sendReport = (req, res) => {
  const { report } = req.body;

  if (!report)
    return res.status(400).send("Le champs doit être rempli.");
  Reports.create({
    report: req.body.report,
    id_user: req.body.id_user
  })
    .then((results) => {
      res.status(200).send(results);
    })
    .catch((err) => {
      res.status(500).send(err);
    })
}