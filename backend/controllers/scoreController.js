const Scores = require("../models").Scores;
const { Op } = require("sequelize");

exports.sendResults = (req, res) => {
  const { id } = req.params;
  const { allScores } = req.body;
  for (let i = 1; i < 11; i++) {
  Scores.create(
    {
      id_match: allScores[i - 1].id_match,
      home: allScores[i - 1].home,
      away: allScores[i - 1].away,
      id_user: id
    }
  )
  .then(() => {
    if (i === 10) res.status(200).send("Score à jour.");
  })
  .catch((err) => {
    res.status(500).send(err);
  });
  }
};

exports.getIfResponse = (req, res) => {
  const { id } = req.params;
  Scores.findAll(
    {
      attributes: [
        'home',
        'away',
        'id_user'
      ],
      where: {
        id_user: id
      }
    }
  ).then(results => {
    res.status(200).send(results);
  })
};

exports.getScore = (req, res) => {
  const { id} = req.params
  Scores.findAll({ 
    where: {
      id_user: id
    }
  })
  .then(results => {
    res.status(200).send(results)
  })
};

exports.purgeResults = (req, res) => {
  Scores.destroy({ 
    where: {
      id_user: {
        // de 0 à 150 user
        [Op.lte]: 150
      }
    }
  })
};
