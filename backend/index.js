require("dotenv").config();
const { API_PORT } = process.env;
const express = require("express");
const cors = require("cors");
const app = express();
const routes = require("./routes/index");

app.use(cors());
app.use(express.json());
app.use("/api", routes);

app.listen(API_PORT, () => {
  console.log("API started");
});
