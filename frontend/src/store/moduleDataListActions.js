import axios from "axios";

export default {
  // Recupère les signalements // vue admin
  async getReporteds({ commit }) {
    return new Promise((resolve, reject) => {
      axios
        .get(process.env.VUE_APP_URL + `api/get-report`, { headers: { "auth-token": localStorage.getItem("token") } })
        .then((response) => {
          const reporteds = response.data;
          commit("SET_REPORTEDS", reporteds);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  // Recupère les signalements par user // vue report-bug
  async getReportPerUser({ commit }, id_user) {
    return new Promise((resolve, reject) => {
      axios
        .get(process.env.VUE_APP_URL + `api/get-report/` + id_user, { headers: { "auth-token": localStorage.getItem("token") } })
        .then((response) => {
          const reportedsPerUser = response.data;
          commit("SET_REPORTEDS_USER", reportedsPerUser);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  // Envoie de signalement // vue report-bug
  async sendReported({ commit }, reported) {
    return new Promise((resolve, reject) => {
      axios
        .post(process.env.VUE_APP_URL + `api/send-report`, reported, { headers: { "auth-token": localStorage.getItem("token") } })
        .then((response) => {
          const reported = response.data;
          commit("ADD_REPORTED", reported);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  
  // Recupère le classement // vue classement
  async getUserPoint({ commit }) {
    return new Promise((resolve, reject) => {
      axios
        .get(process.env.VUE_APP_URL + `api/user-point`, { headers: { "auth-token": localStorage.getItem("token") } })
        .then((response) => {
          const userPoint = response.data;
          commit("SET_USER_POINT", userPoint);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  // Blocked
  async updateBlockOn({ commit }, blocked) {
    return new Promise((resolve, reject) => {
      axios
        .post(
          process.env.VUE_APP_URL + `api/update-blocked`,
          blocked,
          { headers: { "auth-token": localStorage.getItem("token") }}
        )
        .then((response) => {
          const blocked = response.data;
          commit("UPDATE_BLOCKED", blocked);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  async getBlocked({ commit }) {
    return new Promise((resolve, reject) => {
      axios
        .get(process.env.VUE_APP_URL + `api/get-blocked`, {
          headers: { "auth-token": localStorage.getItem("token") }
        })
        .then((response) => {
          const blocked = response.data;
          commit("SET_BLOCKED", blocked);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  }, 
  // Notification
  async getLastFiveNotifications({ commit }) {
    return new Promise((resolve, reject) => {
      axios
        .get(process.env.VUE_APP_URL + `api/get-notification`, {
          headers: { "auth-token": localStorage.getItem("token") }
        })
        .then((response) => {
          const notifications = response.data;
          commit("SET_NOTIFICATIONS", notifications);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  async sendNotification({ commit }, notification) {
    return new Promise((resolve, reject) => {
      axios
        .post(
          process.env.VUE_APP_URL + `api/send-notification`,
          notification,
          {
            headers: { "auth-token": localStorage.getItem("token") }
          }
        )
        .then((response) => {
          const notification = response.data;
          commit("ADD_NOTIFICATION", notification);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  // Users
  async getUserInfo({ commit }, id_user) {
    return new Promise((resolve, reject) => {
      axios
        .get(process.env.VUE_APP_URL + `api/user/` + id_user, {
          headers: { "auth-token": localStorage.getItem("token") },
        })
        .then((response) => {
          const user = response.data;
          commit("SET_USER", user);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  // Scores
  async getIfScores({ commit }, id_user) {
    return new Promise((resolve, reject) => {
      axios
        .get(process.env.VUE_APP_URL + `api/get-score/` + id_user, {
          headers: { "auth-token": localStorage.getItem("token") }
        })
        .then((response) => {
          const scores = response.data;
          commit("SET_SCORES", scores);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  // Response
  async getIfResponse({ commit }, id_user) {
    return new Promise((resolve, reject) => {
      axios
        .get(process.env.VUE_APP_URL + `api/get-response/` + id_user, {
          headers: { "auth-token": localStorage.getItem("token") }
        })
        .then((response) => {
          const scoreExist = response.data;
          commit("SET_SCORES_EXIST", scoreExist);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
}