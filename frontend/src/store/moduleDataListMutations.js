export default {
  SET_REPORTEDS(state, reporteds) {
    state.reporteds = reporteds;
  },
  SET_REPORTEDS_USER(state, reportedsPerUser) {
    state.reportedsPerUser = reportedsPerUser;
  },
  ADD_REPORTED(state, reported) {
    state.reporteds.unshift(reported);
  },
  SET_USER_POINT(state, userPoint) {
    state.userPoint = userPoint;
  },
  SET_BLOCKED(state, blocked) {
    state.blocked = blocked;
  },
  UPDATE_BLOCKED(state, blocked) {
    state.blocked = blocked;
  },
  SET_NOTIFICATIONS(state, notifications) {
    state.notifications = notifications;
  },
  ADD_NOTIFICATION(state, notification) {
    state.notifications.unshift(notification);
  },
  SET_USER(state, user) {
    state.user = user;
  },
  SET_SCORES(state, scores) {
    state.scores = scores;
  },
  SET_SCORES_EXIST(state, scoreExist) {
    state.scoreExist = scoreExist;
  },
}