import state from './moduleDataListState';
import getters from './moduleDataListGetters';
import mutations from './moduleDataListMutations';
import actions from './moduleDataListActions';
import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  getters,
  mutations,
  state,
  actions,
  strict: false
});
