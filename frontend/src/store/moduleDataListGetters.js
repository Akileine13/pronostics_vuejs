export default {
  reporteds: (state) => state.reporteds,
  reportedsPerUser: (state) => state.reportedsPerUser,
  reported: (state) => state.reported,
  userPoint: (state) => state.userPoint,
  blocked: (state) => state.blocked,
  notifications: (state) => state.notifications,
  notification: (state) => state.notification,
  user: (state) => state.user,
  scores: state => state.scores,
  scoreExist: state => state.scoreExist
}