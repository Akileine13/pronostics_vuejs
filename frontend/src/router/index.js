import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/login/Login.vue'
import Register from '../views/login/Register.vue'
import Admin from '../views/admin/Admin.vue'
import UsersActive from '../views/admin/UsersActive.vue'
import Ligue1 from '../views/league/Ligue1.vue'
import Classement from '../views/Classement.vue'
import Profile from '../views/user/Profile.vue'
import Report from '../views/Report.vue'
import verifyToken from './verifyToken'
import checkIfAdmin from './checkIfAdmin'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    beforeEnter: (to, from, next) => {
      verifyToken(next)
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/classement',
    name: 'Classement',
    component: Classement,
    beforeEnter: (to, from, next) => {
      verifyToken(next)
    }
  },
  {
    path: '/admin',
    name: 'Administrator',
    component: Admin,
    beforeEnter: (to, from, next) => {
      verifyToken(next),
      checkIfAdmin(next)
    }
  },
  {
    path: '/users-active',
    name: 'Active users',
    component: UsersActive,
    beforeEnter: (to, from, next) => {
      verifyToken(next),
      checkIfAdmin(next)
    }
  },
  {
    path: '/report-bug',
    name: 'Report bug',
    component: Report,
    beforeEnter: (to, from, next) => {
      verifyToken(next)
    }
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
    beforeEnter: (to, from, next) => {
      verifyToken(next)
    }
  },
  {
    path: '/ligue-1',
    name: 'Ligue1',
    component: Ligue1,
    beforeEnter: (to, from, next) => {
      verifyToken(next)
    }
  },
  {
    path: '*',
    redirect: '/'
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
