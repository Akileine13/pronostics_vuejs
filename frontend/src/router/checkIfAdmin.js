import jwt from "jsonwebtoken";

function checkIfAdmin(next) {
  const saveToken = localStorage.getItem("token");
  if (!saveToken) next({ path: "/login" });
  const verified = jwt.verify(saveToken, process.env.VUE_APP_ACCESS_TOKEN_SECRET);
  
  if(verified._role === 'admin'){ 
    next();
  } else {
    next({ path: "/" });
  }
  
}

export default checkIfAdmin;
