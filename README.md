**Docker**
```
docker-compose up
```

**Frontend**

```
cd frontend
npm i
npm run serve
```

**Backend**

```
cd backend
npm i
sequelize db:migrate
npm run dev
```

*Windows*
```
cd backend
npm i
npx sequelize db:migrate
npm run dev
```